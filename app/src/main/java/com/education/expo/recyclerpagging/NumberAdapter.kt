package com.education.expo.recyclerpagging

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.child.view.*

class NumberAdapter(var a:Context) : RecyclerView.Adapter<NumberAdapter.ViewHolder>() {
    var list=ArrayList<String>()

    class ViewHolder(v: View) :RecyclerView.ViewHolder(v){
        var tv: TextView;
      init {
          tv=itemView.textView
      }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       var v=LayoutInflater.from(a).inflate(R.layout.child,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
       return if (list.size==0) 0
       else 50
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv.text=list[position]
    }

    fun reload(t: List<String>) {
        this.list.clear()
         this.list.addAll(t)
        notifyDataSetChanged()
    }

}
