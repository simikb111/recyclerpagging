package com.education.expo.recyclerpagging

import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitClient private constructor() {
    private val retrofit: Retrofit
    val api: Api
        get() = retrofit.create<Api>(Api::class.java)

    companion object {
        //https://gitlab.com/simikb111/recyclerpagging.git
        //http://waypointsystems.dyndns.org:25410/Api/DemoApi/GetAllNumbers?intStartNo=1&intEndNo=50
        private const val BASE_URL = "http://waypointsystems.dyndns.org:25410/Api/DemoApi/"
        private var mInstance: RetrofitClient? = null

        @get:Synchronized
        val instance: RetrofitClient?
            get() {
                if (mInstance == null) {
                    mInstance = RetrofitClient()
                }
                return mInstance
            }
    }

    init {




        val okHttpClient = OkHttpClient.Builder()
            .build()

        retrofit = Retrofit.Builder()
          .baseUrl(BASE_URL)
           // .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}