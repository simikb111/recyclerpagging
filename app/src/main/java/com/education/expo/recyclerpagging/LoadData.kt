package com.education.expo.recyclerpagging

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoadData{
    companion object{
       public var PAGE_SIZE = 50;
        public var FIRST_SIZE = 1
        public var LAST_SIZE = 50;
       public var FIRST_PAGE = 1;
    }

    fun initialize(callback: MainActivity.Callback<List<String>>)
    {
        var fire= RetrofitClient.instance!!.api
            /*.getNumbers(
              FIRST_PAGE,
               PAGE_SIZE

             )*/.getNumbers(FIRST_SIZE, LAST_SIZE)  ;

        Log.e("outputreq",fire.request().toString())
        fire.enqueue(object : Callback<List<String>?> {
            override fun onResponse(
                call: Call<List<String>?>,
                response: Response<List<String>?>
            ) {
                Log.e("output",response.body().toString())



                if (response.body() != null) {
                    var list: List<kotlin.String> = response.body()!!
                    callback.ok(
                        list
                    )
                }
            }


            override fun onFailure(call: Call<List<String>?>, t: Throwable) {
                Log.e("outputerrorrr",t.message.toString())
            }
        })
    }

    fun onScrol(
        callback: MainActivity.Callback<List<String>>,
        b: Boolean
    )
    {  onGetcount(b)
        var fire= RetrofitClient.instance!!.api
            /*.getNumbers(
              FIRST_PAGE,
               PAGE_SIZE

             )*/.getNumbers(FIRST_SIZE, LAST_SIZE)  ;

        Log.e("outputreq",fire.request().toString())
        fire.enqueue(object : Callback<List<String>?> {
            override fun onResponse(
                call: Call<List<String>?>,
                response: Response<List<String>?>
            ) {
                Log.e("output",response.body().toString())



                if (response.body() != null) {
                    var list: List<kotlin.String> = response.body()!!
                    callback.ok(
                        list
                    )
                }
            }


            override fun onFailure(call: Call<List<String>?>, t: Throwable) {
                Log.e("outputerrorrr",t.message.toString())
            }
        })
    }

    fun onGetcount(b:Boolean){

        if(b){
            FIRST_SIZE=LAST_SIZE
            LAST_SIZE= LAST_SIZE+50
        }
        else if(!b){
            LAST_SIZE= FIRST_SIZE
            FIRST_SIZE= FIRST_SIZE-50
        }
    }
}