package com.education.expo.recyclerpagging;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @GET("GetAllNumbers")
    Call<List<String>> getNumbers(@Query("intStartNo") int s, @Query("intEndNo") int e);

    @GET("GetAllNumbers?intStartNo=1&intEndNo=50")
    Call<List<String>> getNumber2();
// http://waypointsystems.dyndns.org:25410/Api/DemoApi/GetAllNumbers?intStartNo=1&intEndNo=5
// http://waypointsystems.dyndns.org:25410/            GetAllNumbers?intStartNo=1&intEndNo=50,
}