package com.education.expo.recyclerpagging

import android.os.Bundle
import android.util.Log
import android.widget.AbsListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var up=true;
    var adapter:NumberAdapter?=null;
    interface  Callback<T> {
        fun  ok(t:T)
        fun error()
    }

    var callback= object :Callback<List<String>>{
        override fun ok(t: List<String>) {

            adapter!!.reload(t)
            if(up){
                rec.scrollToPosition(0)
            }
            else{
                rec.scrollToPosition(49)
            }


        }

        override fun error() {
          Toast.makeText(this@MainActivity,"Loading error",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inview()

        loadData()
    }

    private fun inview() {
        var isscrolling=false
               rec.layoutManager=LinearLayoutManager(this);
               adapter=NumberAdapter(this@MainActivity)
              rec.adapter=adapter;
              rec.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                var layoutManager=recyclerView.layoutManager as LinearLayoutManager
                val visibleItemCount: Int = layoutManager.getChildCount()
                val totalItemCount: Int = layoutManager.getItemCount()
                val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

                if (dy > 0) //check for scroll down
                {
                    var last= visibleItemCount+firstVisibleItemPosition == totalItemCount-1
                    if (isscrolling) {
                        if (last) {
                            Log.e(
                                "outputpage",
                                "" + totalItemCount + "," + firstVisibleItemPosition + "," + visibleItemCount
                            )
                            up=true;
                            LoadData().onScrol(callback, true)
                            isscrolling = false;
                        }

                    }

                }

               else if (dy < 0)
                {
                    if (isscrolling){
                        if(firstVisibleItemPosition == 1 ){
                            up=false
                            LoadData().onScrol(callback,false)
                                    isscrolling=false;
                        }
                    }


                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isscrolling=true;
                }
                else{ isscrolling=false; }
            }
        })
    }

    private fun loadData() {
    LoadData().initialize(callback)
    }
}